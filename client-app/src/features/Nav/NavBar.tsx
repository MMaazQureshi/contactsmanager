import React, { useContext } from "react";
import { Menu, Segment, Container, Button } from "semantic-ui-react";
import ContactStore from "../../app/stores/ContactStore";
import { NavLink } from "react-router-dom";


export const NavBar:React.FC = () => {
  const contactstore = useContext(ContactStore);
  const {openCreatForm} = contactstore;
  return (
    
    <Segment inverted>
      <Menu  fixed="top" inverted >
     <Container>
        <Menu.Item header as={NavLink} exact to="/"> 
            
            Contact Manager
        </Menu.Item>
        <Menu.Item as={NavLink}  to ="/ContactList" name="Contact List" />
        <Menu.Item >
            <Button positive as={NavLink}  to ="/createcontact"  onClick={openCreatForm} content="Create contact"/>
        </Menu.Item>
        </Container>
      </Menu>
    </Segment>
  );
};
