import React, { useState, useContext, useEffect } from "react";
import { Segment, Form, Button, Grid } from "semantic-ui-react";
import { v4 as uuid } from "uuid";
import ContactStore from "../../../app/stores/ContactStore";
import { observer } from "mobx-react-lite";
import { RouteComponentProps } from "react-router-dom";
import {ContactFormValues } from "../../../app/models/Contact";
import { Form as FinalForm, Field } from "react-final-form";
import { TextInput } from "../../../app/Common/Form/TextInput";

const ContactForm: React.FC<RouteComponentProps<{ id: string }>> = ({
  match,
  history,
}) => {
  const [contact, setContact] = useState(new ContactFormValues());
const [loading,setLoading] = useState(false);

  const handleFinalFormSubmit = (values: any) => {
    const{date,time,...contact} =values
    
    if (!contact.id) {
          let newContact = {
            ...contact,
            id: uuid(),
          };
          createContact(newContact);
        } else {
          editContact(contact);
        }
  };
  const contactStore = useContext(ContactStore);
  const {
    createContact,
    submitting,
    editContact,
    loadContact,
  } = contactStore;
  useEffect(() => {
    if (match.params.id) {
      setLoading(true);
      loadContact(match.params.id).then((contact) => {
         setContact(new ContactFormValues(contact));
      }).finally(()=>{setLoading(false)} );
    }
  }, [
    
    match.params.id,
    loadContact,
  ]);

  return (
    <Grid>
      <Grid.Column width="10">
        <Segment clearing>
          <FinalForm
          initialValues={contact}
            onSubmit={handleFinalFormSubmit}
            render={({ handleSubmit }) => (
              <Form loading={loading}>
                <Field
                  placeholder="Name"
                  name="name"
                  component={TextInput}
                  value={contact.name}
                />
                <Field
                  component={TextInput}
                  rows={2}
                  placeholder="Email"
                  name="email"
                  
                  value={contact.email}
                />
                <Button positive floated="right" loading={submitting} content="Submit" onClick= {handleSubmit} />
                <Button
                  floated="right"
                  content="cancel"
                  onClick={()=>{contact.id? history.push(`/contacts/${contact.id}`):history.push('/contactList')}}
                />
              </Form>
            )}
          ></FinalForm>
        </Segment>
      </Grid.Column>
    </Grid>
  );
};
export default observer(ContactForm);
