import React, { useContext, Fragment, useEffect } from "react";
import { Item, Label} from "semantic-ui-react";
import ContactStore from "../../../app/stores/ContactStore";
import { observer } from "mobx-react-lite";
import { ContactListItem } from "./ContactListItem";
import LoadingComponent  from "../../../app/layout/LoadingComponent";



const ContactList: React.FC = () => {
  const contactStore = useContext(ContactStore);
  const{contactsByAlphabets,loadContacts} = contactStore;
  useEffect(() => {
      loadContacts();
    }, [contactStore,loadContacts]);
  if(contactStore.loadingInitial){

    return<LoadingComponent inverted={true} content="Loading activities..."/>
  }
  else{

    return (
      <Fragment>
      {
        contactsByAlphabets.map(([group,contacts])=>(
          <Fragment key={group}>
          <Label key={group} size='large' color='blue'>
            {group}
          </Label>
          
          <Item.Group divided>
            {contacts.map(contact => (
              <ContactListItem key={contact.id} contact={contact} />
            ))}
          </Item.Group>
        
        </Fragment>
        ))
      }
      
      </Fragment>
      
          
        );}
  }
  
export default observer(ContactList);