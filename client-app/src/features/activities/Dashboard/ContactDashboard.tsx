import React from "react";
import { Grid} from "semantic-ui-react";
import ContactList from "./ContactList";
import { observer } from "mobx-react-lite";



 const ContactDashboard: React.FC= () => {

  return (
    <Grid>
      <Grid.Column width={10}>
       
        <ContactList  />
      </Grid.Column>
    </Grid>
  );
};
export default observer(ContactDashboard);