import React from "react";
import { Item, Button, Segment} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import { IContact } from "../../../app/models/Contact";

export const ContactListItem: React.FC<{ contact: IContact }> = ({
  contact,
}) => {
  return (
    <Segment.Group key={contact.id}>
      <Segment>
        <Item.Group>
          <Item>
            <Item.Image
              size="tiny"
              circular
              src="/assets/user.png"
            ></Item.Image>
            <Item.Content>
              <Item.Header as="a"> {contact.name} </Item.Header>

              <Item.Description>{contact.email}</Item.Description>
              <Button
                as={NavLink}
                to={`/contacts/${contact.id}`}
                floated="right"
                content="view"
                color="blue"
              />
            </Item.Content>
          </Item>
        </Item.Group>
      </Segment>
      </Segment.Group>
  );
};
