import React, { useContext, useEffect } from "react";
import { Grid } from "semantic-ui-react";
import ContactStore from "../../../app/stores/ContactStore";
import { observer } from "mobx-react-lite";
import { RouteComponentProps } from "react-router-dom";
import LoadingComponent from "../../../../../../Contact Manager/client-app/src/app/layout/LoadingComponent"
import { ContactDetailedHeader } from "./ContactDetailedHeader";
import { ContactDetailedInfor } from "./ContactDetailedInfor";

const ContactDetails: React.FC<RouteComponentProps<{ id: string }>> = ({
  match,
  history,
}) => {
  const contactStore = useContext(ContactStore);
  const { contact, loadContact, loadingInitial } = contactStore;

  useEffect(() => {
    loadContact(match.params.id);
  }, [loadContact, match.params.id]);

  if (loadingInitial || !contact)
    return <LoadingComponent inverted={true} content="loading Contact..." />;
  return (
    <Grid>
      <Grid.Column width={10}>
        <ContactDetailedHeader contact={contact} />
        <ContactDetailedInfor contact={contact} />
      </Grid.Column>

      
    </Grid>
  );
};
export default observer(ContactDetails);
