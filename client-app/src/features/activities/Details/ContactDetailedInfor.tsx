import React from 'react'
import { Segment, Grid, Icon } from 'semantic-ui-react'
import { IContact } from '../../../app/models/Contact'
import { format } from 'date-fns'
import { observer } from 'mobx-react-lite'

export const ContactDetailedInfor:React.FC<{contact:IContact}> = ({contact}) => {
    return (
        <Segment.Group>
      <Segment attached='top'>
        <Grid>
          <Grid.Column width={1}>
            <Icon size='large' color='teal' name='info' />
          </Grid.Column>
          <Grid.Column width={15}>
            <p>{contact.email}</p>
          </Grid.Column>
        </Grid>
      </Segment>
      <Segment attached>
        <Grid verticalAlign='middle'>
          <Grid.Column width={1}>
            <Icon name='calendar' size='large' color='teal' />
          </Grid.Column>
          <Grid.Column width={15}>
            <span>Creation Date: {format(contact.dateCreated,'eeee do MMMM YYYY')} </span>
          </Grid.Column>
        </Grid>
      </Segment>
      <Segment attached>
        <Grid verticalAlign='middle'>
          <Grid.Column width={1}>
            <Icon name='calendar' size='large' color='teal' />
          </Grid.Column>
          <Grid.Column width={11}>
            <span>
            Last Modified:  {format(contact.dateModified,'eeee do MMMM YYYY')}
            </span>
          </Grid.Column>
        </Grid>
      </Segment>
    </Segment.Group>
    )
}
export default observer(ContactDetailedInfor);
