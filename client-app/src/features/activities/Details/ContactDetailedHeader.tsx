import React, { useContext } from 'react'
import { Segment, Item ,Image, Header, Button, Form} from 'semantic-ui-react'
import { IContact } from '../../../app/models/Contact';
import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';
import { format } from 'date-fns';
import ContactStore from '../../../app/stores/ContactStore';


const contactImageStyle = {
    filter: 'brightness(30%)'
  };
  
  const contactImageTextStyle = {
    position: 'absolute',
    bottom: '5%',
    left: '5%',
    width: '100%',
    height: 'auto',
    color: 'white'
  };
export const ContactDetailedHeader:React.FC<{contact:IContact}> = ({contact}) => {
  const contactStore = useContext(ContactStore);
  const{deletecontact,submitting} = contactStore;  
  return (
     
        <Segment.Group>
        <Segment basic attached='top' style={{ padding: '0' }}>
          <Image
            src={`/assets/user.png`}
            fluid
            size='medium'
           style={contactImageStyle}
          />
          <Segment style={contactImageTextStyle} basic>
            <Item.Group>
              <Item>
                <Item.Content>
                  <Header
                    size='huge'
                    content={contact.name}
                    style={{ color: 'white' }}
                  />
                  <p>{format(contact.dateCreated,'eeee do MMMM yyyy')}</p>
                </Item.Content>
              </Item>
            </Item.Group>
          </Segment>
        </Segment>
        <Segment clearing attached='bottom'>
         <Form> <Button color="red" floated='right' loading={submitting} onClick={()=>{deletecontact(contact)}} content='Delete Contact'/>
          <Button as={Link} to={`/Edit/${contact.id}`} color='orange' floated='right'>
            Edit Contact
          </Button>
          </Form>
        </Segment>
      </Segment.Group>
    );
}
export default observer(ContactDetailedHeader);