import { observable, action, computed,configure,runInAction } from "mobx";
import { createContext } from "react";
import { IContact } from "../models/Contact";
import agent from "../Api/agent";
import { history } from '../..';
import { toast } from "react-toastify";

configure({enforceActions:"always"})
class ContactStore {
  @observable contactRegistry = new Map<string,IContact>();
  @observable contacts: IContact[] = [];
  @observable loadingInitial = false;
  @observable contact: IContact|null =null;
  @observable editMode = false;
  @observable submitting = false;
  @observable target = "";
  @action loadContacts = async () => {
    this.loadingInitial = true;

    try {
      const contacts = await agent.Contacts.list();
      runInAction(()=>{
        contacts.forEach((contact) => {
          contact.dateCreated = new Date(contact.dateCreated);
          contact.dateModified = new Date(contact.dateModified);
          this.contactRegistry.set(contact.id,contact);
        });
        this.loadingInitial = false;
        
      })
    } catch (error) {
      runInAction(()=>{
        this.loadingInitial = false;
           })
      console.log(error);
    }
  };
  @action openCreatForm= () => {
    this.editMode = true;
    this.contact = null;
  }
  @computed get contactsByAlphabets(){
    var sortedContacts:IContact[] =Array.from(this.contactRegistry.values());
    return this.groupContactsByAlphabets(sortedContacts);
  }
  groupContactsByAlphabets(contacts:IContact[]){
    const sortedContacts = contacts.sort((a,b)=>a.name.localeCompare(b.name));
      
    return Object.entries(sortedContacts.reduce((contacts,contact)=>{
        const Alphabet = contact.name[0];
        contacts[Alphabet] = contacts[Alphabet]?[...contacts[Alphabet],contact]:[contact];
        return contacts;
      },{} as {[key:string]:IContact[]}));
  }
  @action createContact = async (contact: IContact) => {
    this.submitting = true;
    try {
      await agent.Contacts.create(contact);
      runInAction("Create Contact",()=>{
        this.contact = contact;
        history.push(`/contacts/${contact.id}`);
        this.editMode = false;
        this.submitting = false;
        history.push(`/contacts/${contact.id}`);
      })
    
    } catch (error) {
      runInAction("Create Contact",()=>{
        this.submitting = false;

      })
      toast.error("Some error occured!")
      console.log(error.response);
    }
  };
  @action editContact = async (contact: IContact)=>{
    this.submitting = true;
    try {
      await agent.Contacts.update(contact.id,contact);
     runInAction("editing Contact",()=>{
      this.contactRegistry.set(contact.id,contact);
      this.contact = contact;
      this.editMode = false;
      this.submitting = false;
      history.push(`/contacts/${contact.id}`);
     }) 
    } catch (error) {
      runInAction("editing Contact error",()=>{
        this.submitting = false;

      })
     toast.error("Some error occured!")
      console.log(error.response);
    }
  }
  @action deletecontact = async (contact:IContact)=>{
      this.submitting=true;
      try{
        await agent.Contacts.delete(contact.id);
        runInAction("delete Contact",()=>{
       this.contactRegistry.delete(contact.id);
        this.submitting=false;
        history.push("/ContactList");
        })
        
      }
      catch(error)
      {
        console.log(error);
        runInAction("delete contact",()=>{
          this.target='';
        this.submitting=false;
        })
        

      }
  }
  getContact = (id: string) => {
    return this.contactRegistry.get(id);
  }
  @action loadContact = async (id: string) => {
    let contact = this.getContact(id);
    if (contact) {
     runInAction(()=>{this.contact = contact!;}) 
      return contact;
    } else {
      this.loadingInitial = true;
      try {
        contact = await agent.Contacts.details(id);
        runInAction('getting contact',() => {
          contact!.dateCreated = new Date(contact!.dateCreated);
          this.contact = contact!;
         this.contactRegistry.set(contact!.id, contact!);
          this.loadingInitial = false;
        })
        return contact;
      } catch (error) {
        runInAction('get contact error', () => {
          this.loadingInitial = false;
        })
        console.log(error);
      }
    }
  }
    


 
  

  @action selectcontact = (id: string) => {
    this.contact = this.contactRegistry.get(id)!;
    this.editMode = false;
  };
}

export default createContext(new ContactStore());
