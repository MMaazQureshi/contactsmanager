import axios, { AxiosResponse } from 'axios'
import { IContact } from '../models/Contact';
import { history } from '../..';
import { toast } from 'react-toastify';

axios.defaults.baseURL = 'http://localhost:5000/api';
axios.interceptors.response.use(undefined,(error)=>{
    if(error.message=== 'Network Error'&&!error.response)
    {
        toast.error('Network problem')
    }
    const {status,data,config} = error.response ;
    if(status===404){
        history.push('/Notfound')
    }
    if(status===400 && data.errors.hasOwnProperty('id')&&config.method==='get'){
        history.push('/Notfound');
    }
    if(status===500){
        toast.error('Sorry! Internal Server Error')
    }
    throw error;
})
const responseBody = (response:AxiosResponse)=>response.data;
const sleep =(duration:number)=>(response:AxiosResponse)=>
new Promise<AxiosResponse>(resolve=>setTimeout(()=>resolve(response),duration))
const requests={
get : (url:string) => axios.get(url).then(sleep(1000)).then(responseBody),
post : (url:string ,body:{}) => axios.post(url,body).then(sleep(1000)).then(responseBody),
put : (url:string ,body:{}) => axios.put(url,body).then(sleep(1000)).then(responseBody),
delete : (url:string) => axios.delete(url).then(sleep(1000)).then(responseBody),

}
const Contacts ={
    list : () :Promise<IContact[]> => requests.get('Contacts'),
    details :(id:string):Promise<IContact> => requests.get(`Contacts/${id}`),
    create :(contact: IContact) =>requests.post('Contacts',contact),
    update : (id:string,contact: IContact) => requests.put(`Contacts/${id}`,contact),
    delete:(id:string)=>requests.delete(`Contacts/${id}`)
}
export default {
    Contacts
}