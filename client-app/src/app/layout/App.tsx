import React, {Fragment } from "react";
import { NavBar } from "../../features/Nav/NavBar";
import { Container } from "semantic-ui-react";
import ContactDashboard  from "../../features/activities/Dashboard/ContactDashboard";
import {observer} from "mobx-react-lite";
import { Route ,withRouter, RouteComponentProps, Switch} from "react-router-dom";
import ContactForm from "../../features/activities/Forms/ContactForm";
import ContactDetails from "../../features/activities/Details/ContactDetails";
import NotFound from "./NotFound";
import { ToastContainer } from "react-toastify";


const App:React.FC<RouteComponentProps> = ({location}) => {
  // const contactStore = useContext(ContactStore);
  // useEffect(() => {
  //   contactStore.loadContacts();
  // }, [contactStore]); //now equivalent to componenDidMount with second parameter as an array


  return (
    <div>
      <ToastContainer position={'bottom-right'}/>
      <Route exact path="/" render={()=>(
            <Fragment>
            <NavBar/>
             <Container style={{ marginTop: "7em" }}>
               <ContactDashboard/>
              
             </Container>
             </Fragment>
      )} />

      <Route path = {'/(.+)'} render ={()=>(
          <Fragment>
          <NavBar/>
           <Container style={{ marginTop: "7em" }}>
             <Switch>
             <Route exact path="/ContactList" component={ContactDashboard} />
             <Route path="/Contacts/:id" component={ContactDetails} />
             <Route key={location.key} path={["/createcontact","/Edit/:id"]} component={ContactForm}/>
              <Route component={NotFound}/>
             </Switch>
            
           </Container>
           </Fragment>
      )

      } />
     
    </div>
  );
};

  
export default withRouter(observer(App));
