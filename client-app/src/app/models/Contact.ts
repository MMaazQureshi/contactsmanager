import { runInAction } from "mobx";

export interface IContact{
    id:string,
    name:string;
    email : string,
    dateCreated :Date,
    dateModified:Date,
    
}
export interface IContactFormValues extends Partial<IContact>{


}
export class ContactFormValues implements IContactFormValues{
    id?: string= undefined;
    email:string = ""
    name:string = "";
    dateCreated?:Date=undefined;
    dateModified?:Date=undefined;
    constructor(init?:IContactFormValues){
        runInAction("setObject",()=>{Object.assign(this,init)});
        
        
    }
} 