using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Activites;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using Application.Contacts;

namespace API.Controllers
{
      [ApiController]
    [Route("api/[controller]")]
    public class ContactsController : ControllerBase
    {
        private readonly IMediator mediator;

        public ContactsController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [HttpGet]
        public async Task<ActionResult<List<Contact>>> List()
        {
            return await mediator.Send(new List.Query());
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<Contact>> Details(Guid id)
        {
            return await mediator.Send(new Details.Query { Id = id });
        }
        [HttpPost]
        public async Task<ActionResult<Unit>> Create(Create.Command Command)
        {
            return await mediator.Send(Command);
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<Unit>> Edit(Guid id, Edit.Command command)
        {
            command.Id = id;
            return await mediator.Send(command);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> Delete(Guid id)
        {
            return await mediator.Send(new Delete.Command { Id = id });
        }
    }
}