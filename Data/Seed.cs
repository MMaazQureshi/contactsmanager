using System;
using System.Collections.Generic;
using System.Linq;
using Domain;

namespace Data
{
    public class Seed
    {
        public static void SeedData(DataContext context)
        {
  if(!context.Contacts.Any()){
                    List<Contact> activities = new List<Contact>(){
                       new Contact
{
    Name = "Friend1",
    Email="abc@gmail.com",
    DateModified = DateTime.Now.AddMonths(-2),
     DateCreated = DateTime.Now.AddMonths(-4),
    
},
new Contact
{
     Name = "Friend2",
    Email="abc2@gmail.com",
    DateModified = DateTime.Now.AddMonths(-1),
   DateCreated = DateTime.Now.AddMonths(-3),
}
 } ;
                    context.Contacts.AddRange(activities);
                    context.SaveChanges();
                }
        }
    }
}