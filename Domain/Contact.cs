using System;

namespace Domain
{
    public class Contact
    {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public DateTime DateModified { get; set; }
            public DateTime DateCreated { get; set; }  


    }
}