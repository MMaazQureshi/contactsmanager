using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Data;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Contacts
{
    public class List
    {
        public class Query:IRequest<List<Contact>> {}

        public class Handler : IRequestHandler<Query,List<Contact>>
        {public DataContext Context { get; }
            public Handler(DataContext context)
            {
                this.Context = context;
            }

            

            public async Task<List<Contact>> Handle(Query request, CancellationToken cancellationToken)
            {
                var contacts = await Context.Contacts.ToListAsync();

                    return contacts;
            }
        }
    }
}