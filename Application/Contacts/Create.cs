using System;
using System.Threading;
using System.Threading.Tasks;
using Data;
using Domain;
using MediatR;
using System.ComponentModel.DataAnnotations;
using FluentValidation;
namespace Application.Activites
{
    public class Create
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public DateTime DateModified { get; set; }
            public DateTime DateCreated { get; set; }

        

        }
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Name).NotEmpty();
                RuleFor(x => x.Email).NotEmpty();


            }
        }
        public class handler : IRequestHandler<Command>
        {
            private readonly DataContext context;

            public handler(DataContext context)
            {
                this.context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var activity = new Contact
                {
                    Id = request.Id,
                  Name   = request.Name,
                    Email = request.Email,
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now

                };
                context.Contacts.Add(activity);
                var success = await context.SaveChangesAsync() > 0;
                if (success) return Unit.Value;
                throw new Exception("Problem saving changes");

            }
        }
    }
}