using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Data;
using FluentValidation;
using MediatR;

namespace Application.Contacts
{
    public class Edit
    {
        public class Command : IRequest
        {
            //properties of object
           public Guid Id { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public DateTime DateModified { get; set; }
            public DateTime DateCreated { get; set; }
        }
         public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                 RuleFor(x => x.Name).NotEmpty();
                RuleFor(x => x.Email).NotEmpty();



            }
        }
        public class handler : IRequestHandler<Command>
        {
            private readonly DataContext context;

            public handler(DataContext context)
            {
                this.context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var contact = await context.Contacts.FindAsync(request.Id);
                 if (contact == null)
                    throw new RestException(HttpStatusCode.NotFound,new { contact= "contact not found" });
                contact.Name = request.Name ?? contact.Name;
                contact.Email = request.Email ?? contact.Email;
               contact.DateModified = DateTime.Now;
                var success = await context.SaveChangesAsync() > 0;
                if (success) return Unit.Value;
                throw new Exception("Problem saving changes");

            }
        }
    }
}