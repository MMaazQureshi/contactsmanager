using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Data;
using Domain;
using MediatR;

namespace Application.Activites
{
    public class Details
    {
        public class Query : IRequest<Contact>
        {
            public Guid Id { get; set; }
        }
        public class Handler : IRequestHandler<Query, Contact>
        {
            private readonly DataContext context;

            public Handler(DataContext context)
            {
                this.context = context;
            }


            public async Task<Contact> Handle(Query request, CancellationToken cancellationToken)
            {
                var contact = await context.Contacts.FindAsync(request.Id);
                 if (contact == null)
                    throw new RestException(HttpStatusCode.NotFound,new { contact= "Contact not found" });
                return contact;

            }
        }
    }
}